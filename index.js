// Nomor 1
// Luas Persegi Panjang
let luasPersegipanjang = function(p,l)
{
    return p*l;
}

console.log('function = ', luasPersegipanjang(10,5));

let luas = (p,l) => {return p*l;};
console.log('Arrow = ', luas(10,5));

// Keliling Persegi Panjang

let KelilingPersegipanjang = function(p,l)
{
    return 2*(p+l);
}

console.log('function = ', KelilingPersegipanjang(10,5));

let keliling = (p,l) => {return 2*(p+l);};
console.log('Arrow = ', keliling(10,5));

// Nomor 2
// Soal
/* 
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 
*/

// Jawaban
const newFunction = (firstName, lastName) => {
    console.log(firstName + " " + lastName);
};
newFunction("William","Imoh");

// Nomor 3
// Soal
/* const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
*/

// Jawaban
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  };

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

// Nomor 4
// Soal
/*
Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)
*/

// Jawaban
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

let combinedArray = [... west, ... east]
console.log(combinedArray)

// Nomor 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

const theString = (`${planet}, ${view}, ${before}`);
console.log(theString)